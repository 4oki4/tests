from django.db import models


class List(models.Model):
    """список"""
    pass


class Item(models.Model):
    """элемеент списка"""
    text = models.TextField(default='')
    list = models.TextField(List, default=None)
